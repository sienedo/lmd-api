<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class API extends CI_Controller {

	public function get_rate_and_schedule(){
    header("Content-Type:application/json");
    $headers = apache_request_headers();

    // $data_string=file_get_contents('php://input');
    $data = json_decode(file_get_contents('php://input'),true);
    if($headers['API-Key']=='MhPkSpUrWuZw3y6B8DbGdJfNjQmSqVsXu2x4z7C9EbHeKgNkRn'){
       if($data['party_id']=='4u7x!A%D*G-JaNdRgUkXp2s5v8y/B?E(H+MbPeShVmYq3t6w9z'){
          if($data['token']==')H@McQfTjWnZr4u7x!A%D*F-JaNdRgUkXp2s5v8y/B?E(H+KbP'){
            $datetime2= new datetime();
    $datetime2= $datetime2->format('Ymdhis');
    $mt2 = explode(' ', microtime());
    $mls2 = ((int)$mt2[1])*1000+((int)round($mt2[0] * 1000));
    $mls2 = substr($mls2, -3);
    $code2="APILG";
    $pid_code2=$code2.$datetime2.$mls2;
    $simpanpid_code=$pid_code2;
    // var_dump($data);
    // echo $data['email'];
    if($data['email']&&$data['no_phone']){
      if($data['cargo_detail']=='fcl'){
        $container_flag=true;
      }elseif($data['cargo_detail']=='lcl'){
        $container_flag=false;
      }
      $ip=$this->getRealIpAddr();
      $insertdata = array(
        'pid' => $simpanpid_code,
        'email'=>$data['email'],
        'no_phone'=>$data['no_phone'],
        'transport_type'=>$data['transport_type'],
        'cargo_detail'=>$data['cargo_detail'],
        'movement'=>$data['movement'],
        'pickup_date'=>$data['pickup_date'],
        'grandquantity_total'=>$data['quantity_total'],
        'grandcbm_total'=>$data['cbm_total'],
        'grandweight_total'=>$data['weight_total'],
        'container_flag'=> $container_flag,
        'genco_flag'=>$data['genco'],
        'perishable_flag'=>$data['perishable'],
        'container'=>$data['container'],
        'origin'=>$data['origin'],
        'dest'=>$data['dest'],
        'ip'=>$ip
      );
      $this->db->insert('api_rate_log', $insertdata);
      if($data['transport_type']=="ocean"){
        if($data['cargo_detail']=='lcl'){
          if(empty($data['dest'])||empty($data['origin'])||empty($data['cargo_detail'])||empty($data['movement'])||empty($data['pickup_date'])||empty($data['quantity_total'])||empty($data['weight_total'])||empty($data['cbm_total'])){
            $response = array('response' => '400' );
            echo json_encode($response);
            exit();
          }
          if($data['movement']!="ptp"){
          $reks = array();
          $response = array(
            'response' => '200',
            'count'=>sizeof($resk),
            'data'=>$reks, 
          );
          echo json_encode($response);
          exit();
        }
          if($data['genco']=='1'){
            $tempcommodity='Genco';
          }elseif($data['perishable']=='1'){
            $tempcommodity='PER';
          }
          $this->db->select('rate_ocean_full.*,a.city as kota_origin,b.city as kota_destination');
          $this->db->from('rate_ocean_full');
          $this->db->from('ms_seaport as b');
          $this->db->where('origin', $data['origin']);
          $this->db->like('destination', $data['dest'], 'BEFORE');
          $this->db->where('date(valid_from) <=', $data['pickup_date']);
          $this->db->where('date(valid_to) >=', $data['pickup_date']);
          $this->db->where('b.unlocode_code', $data['dest']);
          $this->db->where('rate_ocean_full.container', 'RATE');
          $this->db->like('rate_ocean_full.id_gscr', $tempcommodity, 'BOTH');
          $this->db->join('ms_seaport as a', 'rate_ocean_full.origin=a.unlocode_code', 'left');
          $response_awal=$this->db->get()->result_array();
          $vendor_code = array();
          $response2 = array();
          foreach ($response_awal as $key => $value) {
            $counterawalfilter=0;
            $cwt=0;
            if(($data['weight_total']/1000)>$data['cbm_total']){
              $cwt=$data['weight_total']/1000;
            }elseif (($data['weight_total']/1000)<$data['cbm_total']) {
              $cwt=$data['cbm_total'];
            }else{
              $cwt=$data['weight_total']/1000;
            }
            if($cwt<=$value['minimum']){
              $value['buying_rate']=$value['buying_rate']*2;
            }elseif ($cwt>$value['minimum']) {
              $value['buying_rate']=$value['buying_rate']*$cwt;
            }
            foreach ($vendor_code as $key2 => $value2) {
              if($value2==$value['id_vendor']){
                $counterawalfilter+=1;
              }
            }
            if($counterawalfilter==0){
              array_push($vendor_code, $value['id_vendor']);
              $inserttemp = array(
                'id_vendor_services'=>$value['id_vendor_services'],
                'id_vendor'=>$value['id_vendor'],
                'name_vendor'=>$value['vendor_name'],
                'rate'=>$value['buying_rate'],
                'currency'=>$value['currency'],
                'transit_time'=>$value['transit_time'],
                'time_unit'=>$value['time_unit'],
                'status_quotation'=>'available',
                'valid_from'=>$value['valid_from'],
                'valid_to'=>$value['valid_to'],
                'remark'=>$value['remark']

              );
              // $this->db->insert('customer_temp_quotation', $inserttemp);
              array_push($response2, $inserttemp);
            }
          }

        }elseif($data['cargo_detail']=='fcl'){
          if(empty($data['dest'])||empty($data['origin'])||empty($data['cargo_detail'])||empty($data['movement'])||empty($data['pickup_date'])||empty($data['quantity_total'])||empty($data['weight_total'])||empty($data['cbm_total'])){
            $response = array('response' => '400' );
            echo json_encode($response);
            exit();
          }
          if($data['movement']!="ptp"){
          $reks = array();
          $response = array(
            'response' => '200',
            'count'=>sizeof($resk),
            'data'=>$reks, 
          );
          echo json_encode($response);
          exit();
        }
          if($data['genco']=='1'){
            $tempcommodity='Genco';
          }elseif($data['perishable']=='1'){
            $tempcommodity='PER';
          }
          $this->db->select('rate_ocean_full.*,a.city as kota_origin,b.city as kota_destination');
          $this->db->from('rate_ocean_full');
          $this->db->from('ms_seaport as b');
          $this->db->where('origin', $data['origin']);
          $this->db->like('destination', $data['dest'], 'BEFORE');
          $this->db->where('date(valid_from) <=', $data['pickup_date']);
          $this->db->where('date(valid_to) >=', $data['pickup_date']);
          $this->db->where('b.unlocode_code', $data['dest']);
          $this->db->where('rate_ocean_full.container', $data['container']);
          $this->db->like('rate_ocean_full.id_gscr', $tempcommodity, 'BOTH');
          $this->db->join('ms_seaport as a', 'rate_ocean_full.origin=a.unlocode_code', 'left');
          $response_awal=$this->db->get()->result_array();
          $vendor_id = array();
          $response2 = array();

          foreach ($response_awal as $key => $value) {
            $counterawalfilter=0;
            $value['buying_rate']=$value['buying_rate']*$data['quantity_total'];
            foreach ($vendor_id as $key2 => $value2) {
              if($value2==$value['id_vendor']){
                $counterawalfilter+=1;
              }
            }
            if($counterawalfilter==0){
              array_push($vendor_id, $value['id_vendor']);
              $inserttemp = array(
                'id_vendor_services'=>$value['id_vendor_services'],
                'id_vendor'=>$value['id_vendor'],
                'name_vendor'=>$value['vendor_name'],
                'rate'=>$value['buying_rate'],
                'currency'=>$value['currency'],
                'transit_time'=>$value['transit_time'],
                'time_unit'=>$value['time_unit'],
                'status_quotation'=>'available',
                'valid_from'=>$value['valid_from'],
                'valid_to'=>$value['valid_to'],
                'remark'=>$value['remark']

              );
              array_push($response2, $inserttemp);
            }
          }

        }

      }elseif($data['transport_type']=="air"){
        if(empty($data['dest'])||empty($data['origin'])||empty($data['movement'])||empty($data['pickup_date'])||empty($data['quantity_total'])||empty($data['weight_total'])||empty($data['cbm_total'])){
          $response = array('response' => '400' );
          echo json_encode($response);
          exit();
        }
        if($data['movement']!="ptp"){
          $reks = array();
          $response = array(
            'response' => '200',
            'count'=>sizeof($resk),
            'data'=>$reks, 
          );
          echo json_encode($response);
          exit();
        }
          if($data['genco']=='1'){
            $kode_cari='Genco';
          }elseif($data['perishable']=='1'){
            $kode_cari='PER';
          }else{
            $kode_cari='Genco';
          }
          // echo $kode_cari;
          // exit();
          $this->db->select('rate_air_full.*,a.location as kota_origin,b.location as kota_destination,weight,formula');
          $this->db->from('rate_air_full');
          $this->db->where('origin', $data['origin']);
          $this->db->where('destination', $data['dest']);
          $this->db->where('date(valid_from) <=', $data['pickup_date']);
          $this->db->where('date(valid_to) >=', $data['pickup_date']);
          $this->db->like('rate_air_full.id_gscr', $kode_cari, 'BOTH');
          $this->db->join('ms_airport as a', 'rate_air_full.origin=a.iata_code', 'left');
          $this->db->join('ms_airport as b', 'rate_air_full.destination=b.iata_code', 'left');
          $this->db->order_by('rate_air_full.weight', 'desc');
          $response_awal=$this->db->get()->result_array();
          $vendor_id = array();
          $response2 = array();
          foreach ($response_awal as $key => $value) {
            if(empty($value['formula'])){

            }else{
              $counterawalfilter=0;
              $cwt=0;
              if($data['weight_total']>$data['cbm_total']){
                $cwt=$data['weight_total'];
              }elseif ($data['weight_total']<$data['cbm_total']) {
                $cwt=$data['cbm_total'];
              }else{
                $cwt=$data['weight_total'];
              }
              // echo $cwt;
              // exit();
              foreach ($vendor_id as $key2 => $value2) {
                if($value['id_vendor']==$value2){
                  $counterawalfilter+=1;
                }
              }
              if($counterawalfilter==0){
                if($value['formula']==">="){
                  if($cwt>=$value['weight']){
                    array_push($vendor_id, $value['id_vendor']);
                    $value['buying_rate']=$value['buying_rate']*$cwt;
                    $inserttemp = array(
                      'pid_vsgc_rate'=> $value['pid_vsgc_rate'],
                      'id_vendor_services'=>$value['id_vendor_services'],
                      'id_vendor'=>$value['id_vendor'],
                      'name_vendor'=>$value['vendor_name'],
                      'rate'=>$value['buying_rate'],
                      'currency'=>$value['currency'],
                      'transit_time'=>$value['transit_time'],
                      'time_unit'=>$value['time_unit'],
                      'status_quotation'=>'available',
                      'valid_from'=>$value['valid_from'],
                      'valid_to'=>$value['valid_to'],
                      'remark'=>$value['remark']

                    );
                    array_push($response2, $inserttemp);
                    // var_dump($inserttemp);
                    // array_push($inserttemp, $value2['text_formula']);
                    // array_push($inserttemp, $cwt);
                    // echo json_encode($inserttemp);
                    // $value->buying_rate=number_format($value->buying_rate,2,",",".");
                  }
                }elseif($value['formula']==">"){
                  if($cwt>$value['weight']){
                    array_push($vendor_id, $value['id_vendor']);
                    $value['buying_rate']=$value['buying_rate']*$cwt;
                    $inserttemp = array(
                      'pid_vsgc_rate'=> $value['pid_vsgc_rate'],
                      'id_vendor_services'=>$value['id_vendor_services'],
                      'id_vendor'=>$value['id_vendor'],
                      'name_vendor'=>$value['vendor_name'],
                      'rate'=>$value['buying_rate'],
                      'currency'=>$value['currency'],
                      'transit_time'=>$value['transit_time'],
                      'time_unit'=>$value['time_unit'],
                      'status_quotation'=>'available',
                      'valid_from'=>$value['valid_from'],
                      'valid_to'=>$value['valid_to'],
                      'remark'=>$value['remark']

                    );
                    array_push($response2, $inserttemp);
                  }
                }elseif($value['formula']=="<="){
                  if($cwt<=$value['weight']){
                    array_push($vendor_id, $value['id_vendor']);
                    $value['buying_rate']=$value['buying_rate']*$cwt;
                    $inserttemp = array(
                      'pid_vsgc_rate'=> $value['pid_vsgc_rate'],
                      'id_vendor_services'=>$value['id_vendor_services'],
                      'id_vendor'=>$value['id_vendor'],
                      'name_vendor'=>$value['vendor_name'],
                      'rate'=>$value['buying_rate'],
                      'currency'=>$value['currency'],
                      'transit_time'=>$value['transit_time'],
                      'time_unit'=>$value['time_unit'],
                      'status_quotation'=>'available',
                      'valid_from'=>$value['valid_from'],
                      'valid_to'=>$value['valid_to'],
                      'remark'=>$value['remark']

                    );
                    array_push($response2, $inserttemp);
                  }
                }elseif($value['formula']=="<"){
                  if($cwt<$value['weight']){
                    array_push($vendor_id, $value['id_vendor']);
                    $value['buying_rate']=$value['buying_rate']*$cwt;
                    $inserttemp = array(
                      'pid_vsgc_rate'=> $value['pid_vsgc_rate'],
                      'id_vendor_services'=>$value['id_vendor_services'],
                      'id_vendor'=>$value['id_vendor'],
                      'name_vendor'=>$value['vendor_name'],
                      'rate'=>$value['buying_rate'],
                      'currency'=>$value['currency'],
                      'transit_time'=>$value['transit_time'],
                      'time_unit'=>$value['time_unit'],
                      'status_quotation'=>'available',
                      'valid_from'=>$value['valid_from'],
                      'valid_to'=>$value['valid_to'],
                      'remark'=>$value['remark']

                    );
                    array_push($response2, $inserttemp);
                  }
                }
              }
            }
      }

      }else{
        $response = array(
          "response"=>"400", 
        );
        
    }

      // echo json_encode($response);
    }else{
      $response = array(
        "response"=>"300", 
      );
      // echo json_encode($response);
    }
          }else{
            $response = array(
              "response"=>"501", 
            );
          }
          
       }else{
        $response = array(
          "response"=>"505", 
        );
       } 
       
    }else{
      $response = array(
        "response"=>"503", 
      );
    }
    if(empty($response2)){
      echo json_encode($response);
    }else{
      $response=array(
        'response' => '200',
        'count'=>sizeof($response2),
        'data'=>$response2
       );
      echo json_encode($response);
    }
    
  }

    function getRealIpAddr()
  {
      if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
      {
        $ip=$_SERVER['HTTP_CLIENT_IP'];
      }
      elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
      {
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
      }
      else
      {
        $ip=$_SERVER['REMOTE_ADDR'];
      }
      return $ip;
  }	
}
