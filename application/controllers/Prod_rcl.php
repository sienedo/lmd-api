<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prod extends CI_Controller {


	public function index()
	{
		header("Access-Control-Allow-Origin: *");

	}

	public function get_all(){
		header("Content-Type:application/json");
		$headers = apache_request_headers();
		$secret="5ADAEBA8BF4F44D6EEF86CD333DCC";
		$Data = json_decode(file_get_contents('php://input'),true);
		$start = microtime(true);
		if(isset($Data['apikey'])){
        $api_key = $Data['apikey'];
        if($api_key == $secret) 
	        {	
	        	$datetime  = new DateTime();
		        $date = $datetime->format('Y-m-d');
		        $time = $datetime->format('h:i:s');
		        $timestamp = $date.'T'.$time.'+07:00';
		        // $timestamp = $date.' '.$time;
				// $this->db->select("'".$timestamp."' as timestamp,
				// 	b.manifest_number,
				// 	a.conn_xsys,
				// 	a.conn_vendor,
				// 	a.conn_customer,
				// 	a.customer_name,
				// 	a.customer_zipcode,
				// 	a.customer_address_1,
				// 	a.customer_tel_no,
				// 	a.cod,
				// 	a.item_value,
				// 	a.item_desc,
				// 	a.package_weight,
				// 	a.package_quantity
				// 	");
				$this->db->select("'".$timestamp."' as datetime,
					a.conn_customer, 
					a.conn_xsys as tracking_number,
					a.shipper_name,
					a.shipper_address_1 as shipper_address,
					'02150205100' as shipper_phone,
					a.shipper_mail as shipper_email,
					a.customer_name as consignee_name,
					a.customer_address_1 as consignee_address,
					a.customer_state as consignee_province,
					a.customer_city as consignee_city,
					a.customer_zipcode as consignee_zipcode,
					a.customer_tel_no as consignee_phone,
					a.customer_email as consignee_email,
					case when a.chargeable_weight=0
						then a.package_weight 
						when a.chargeable_weight is not null
						then a.chargeable_weight
						when a.package_weight is not null
						then a.package_weight 
						else 0
						end as weight,
					case when a.length = 0 
						then '1'
						when a.length is null
						then '1'
						else a.length
					end as length,
					case when a.width = 0 
						then '1'
						when a.width is null
						then '1'
						else a.width
					end as width,
					case when a.height = 0 
						then '1'
						when a.height is null
						then '1'
						else a.height
					end as height,
					a.package_quantity as qty,
					a.item_desc,
					'REG' as service_type,
					case when a.cod is null
					then 'false'
					when a.cod =0
					then 'false'
					when a.cod =1
					then 'true'
					end as is_cod,
					case when a.item_value is null 
					then '0'
					else a.item_value
					end as item_value",false);
				$this->db->from('ms_manifest as a');
				$this->db->join('manifest_vendor as b', 'a.conn_xsys=b.conn_xsys', 'inner');
				// $this->db->join('colly_manifest as c', 'a.conn_customer=c.conn_customer', 'inner');
				// $this->db->join('ms_tlc_original as d', 'a.customer_zipcode = d.postal_code', 'left');
				$this->db->where('b.vendor_code', '002');
				// $this->db->where('c.is_active', '1');
				$this->db->where('b.manifest_number', $Data['manifestno']);
				$data=$this->db->get()->result();


				
				foreach ($data as $key => $value) {
					$this->db->select('
					item_desc as item_description,
					package_quantity as qty,
					');
					$this->db->from('ms_manifest');
					$this->db->where('conn_customer', $value->conn_customer);
					$value->details=$this->db->get()->result();
					unset($value->conn_customer);
				}
				$time_elapsed_secs = microtime(true) - $start;
				$response = array(
					'success' => true,
					'message' => 'Success',
					'data' => $data,
					'response_time'=>$time_elapsed_secs
				 );
	        }else{
	        	$response = array(
					'response' => 'error api key');
	        }
	    }else{
	    	$response = array(
				'response' => 'error apikey not found');
	    }
		
		
		echo json_encode($response);
	}

	public function dorong_penyedia_layanan(){
		header("Content-Type:application/json");
		$headers = apache_request_headers();
		$data_string=file_get_contents('php://input');
		$Data = json_decode(file_get_contents('php://input'),true);
		$token=$Data['token'];
		$data=$Data['data'];
		$this->db->select('vendor_name,api_key,api_token');
		$this->db->where('api_key', $headers['api_key']);
		$this->db->where('api_token', $token);
		$this->db->from('ms_vendor');
		$data_fromdb=$this->db->get()->row_array();
		$vendor_name=$data_fromdb['vendor_name'];
		$secret=$data_fromdb['api_key'];
		$tokenize=$data_fromdb['api_token'];
		
		if(isset($headers['api_key'])){
        $api_key = $headers['api_key'];
        if($api_key == $secret) {
	        	if($token==$tokenize){
	        		$data=json_decode(json_encode($data),true);
	        		
	        		
	        		foreach ($data as $key => $value) {
	        			$datetime3= new datetime();
		                $datetime3= $datetime3->format('Ymdhis');
	        			$mt3 = explode(' ', microtime());
		                $mls3 = ((int)$mt3[1])*1000+((int)round($mt3[0] * 1000));
		                $mls3 = substr($mls3, -3);
		                $code3="LGSTS";
		                $pid_code3=$code3.$datetime3.$mls3;
		                // $string_gabungan=$value['conn_xsys'].' '.$value['conn_vendor'].' '.$value['status_code'].' '.$value['reason_code'].' '.$value['last_update'].' '.$value['remarks'];
	        			$db_logs = array(
	        				'pid' => $pid_code3,
	        				'logs'=>$data_string.$vendor_name,
	        				'datetime'=>$datetime3,
	        				'table_code'=>$code3
	        			 );
	        			$this->db->insert('api_logs', $db_logs);
	        			if($value['status_code']!='220'){
		        			$update=array(
			        			'conn_vendor'=>$value['conn_vendor'],
			        			'last_status'=>$value['status_code'],
			        			'remark'=>$value['remarks'],
								'modified_by'=>$vendor_name 
			        		);
			        		$this->db->where('conn_xsys', $value['conn_xsys']);
			        		$this->db->update('ms_manifest', $update);
		        		}
		        		$this->db->select('conn_customer');
		        		$this->db->from('ms_manifest');
		        		$this->db->where('conn_xsys', $value['conn_xsys']);
		        		$conn_customer=$this->db->get()->row()->conn_customer;


		        		$datetime2= new datetime();
		                $datetime2= $datetime2->format('Ymdhis');
		                if($value['status_code']!='220'){
		                	$mt2 = explode(' ', microtime());
			                $mls2 = ((int)$mt2[1])*1000+((int)round($mt2[0] * 1000));
			                $mls2 = substr($mls2, -3);
			                $code2="CNSTS";
			                $pid_code2=$code2.$datetime2.$mls2;
			        		$insertan= array(
			        			'pid' => $pid_code2,
			        			'conn_customer' => $conn_customer,
			        			'status_code'=>$value['status_code'],
			        			'last_update'=>$value['last_update'],
			        			'create_by'=>$vendor_name,
			        			'reason_code'=>$value['reason_code'],
			        			'table_code'=>'CNSTS',
			        			'remarks'=>$value['remarks'],
							'is_deliver'=>'0'
			        			 );
			        		$this->db->insert('connote_status', $insertan);
		                }
		                
		        		$respon = array(
						'response' => 'success' 
						);
	        		}
	        		
	        	}else{
	        		//jika salah token
	        		$respon = array(
						'response' => 'error token' 
						);
	        	}
	        }else{
	        	//jika salah api key
	        	$respon = array(
						'response' => 'error api key' 
						);
	        }
	        //jika api key kosong
	        echo json_encode($respon);
		}
		
	}

	public function order_number(){
		header("Content-Type:application/json");
		$headers = apache_request_headers();
		$Data = json_decode(file_get_contents('php://input'),true);
		// $client_token="prodv3.9d7a3252d155a346713db9c1d75729ce5";
		// $client_token="prodv3.9d1b363c59c3782194c4a7454199f3b11";
		$client_token=$Data['token'];
		$amount=$Data['amount'];
		if(isset($headers['api_key'])&&isset($headers['party_id'])){
			$thisdate=new datetime();
			$thisdate= $thisdate->format('Y-m');
			$this->db->select('code');
			$this->db->from('ms_year_month');
			$this->db->where('year_month', $thisdate);
			$code=$this->db->get()->row();
			// echo $code->code;
			$this->db->select('account_code');
			$this->db->from('customer_account');
			$this->db->where('api_token', $client_token);
			$this->db->where('api_key', $headers['api_key']);
			$this->db->where('party_id', $headers['party_id']);
			$customer=$this->db->get()->row();
			// echo $customer->account_code;
			if(empty($customer->account_code)){
				$response = array('response' => 'error' );
				$json=json_encode($response);
				echo $json;
				exit();
			}
			$susunan=$customer->account_code.'X'.$code->code;
			$this->db->select('conn_xsys');
			$this->db->from('ms_manifest');
			$this->db->like('conn_xsys', $susunan, 'after');
			$starting_order=$this->db->get()->row();
			// echo $this->db->last_query();    
			// echo count($starting_order);
			$output = array();
			// print_r($starting_order);
			if(empty($starting_order)){
				// echo "kosong";
				$number=0;
				$number+=1;
				for ($i=0; $i < $amount ; $i++) {
					if($number<=9){
						$fixnumber=$susunan."A0000000".$number;
					}else if($number<=99){
						$fixnumber=$susunan."A000000".$number;
					}else if($number<=999){
						$fixnumber=$susunan."A00000".$number;
					}else if($number<=9999){
						$fixnumber=$susunan."A0000".$number;
					}else if($number<=99999){
						$fixnumber=$susunan."A000".$number;
					}else if($number<=999999){
						$fixnumber=$susunan."A00".$number;
					}else if($number<=9999999){
						$fixnumber=$susunan."A0".$number;
					}else if($number<=99999999){
						$fixnumber=$susunan."A".$number;
					} 
					// echo $fixnumber;
					$isi = array('conn_xsys' => $fixnumber );
					array_push($output, $isi);
					// echo "<br/>";
					$number+=1;
				}

			}else{
				// echo "ada";
				$number=explode($susunan, $starting_order->conn_xsys);
				$number2=explode('A', $number[1]);
				// print_r($number2);
				$numbertocount=(int)$number2[1];
				// echo $numbertocount;
				$numbertocount+=1;
				for ($i=0; $i < $amount ; $i++) {
					if($numbertocount<=9){
						$fixnumber=$susunan."A0000000".$numbertocount;
					}else if($numbertocount<=99){
						$fixnumber=$susunan."A000000".$numbertocount;
					}else if($numbertocount<=999){
						$fixnumber=$susunan."A00000".$numbertocount;
					}else if($numbertocount<=9999){
						$fixnumber=$susunan."A0000".$numbertocount;
					}else if($numbertocount<=99999){
						$fixnumber=$susunan."A000".$numbertocount;
					}else if($numbertocount<=999999){
						$fixnumber=$susunan."A00".$numbertocount;
					}else if($numbertocount<=9999999){
						$fixnumber=$susunan."A0".$numbertocount;
					}else if($numbertocount<=99999999){
						$fixnumber=$susunan."A".$numbertocount;
					} 
					// echo $fixnumber;
					$isi = array('conn_xsys' => $fixnumber );
					array_push($output, $isi);
					// echo "<br/>";
					$numbertocount+=1;
				}
			}

			// print_r($output);
			$json=json_encode($output);
			echo $json;
		}else{
			$response = array('response' => 'error' );
			$json=json_encode($response);
			echo $json;
		}
	}
}
