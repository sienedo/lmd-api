<?php class Wms_model extends CI_Model{

  public function get_data(){
    $this->load->database();
		$query = $this->db->query("select * from name_list");
    $i=1;
    foreach ($query->result() as $row)
    {
        $datquery['a'.$i]['id'] = $row->id;
        $datquery['a'.$i]['nama'] = $row->nama;
        $i++;
    }
    return $datquery;


  }

  public function post_data($data1){
    // $data1 = extract($data1);
    $this->load->database();
    $query = $this->db->query("insert into name_list(nama) values('".$data1."')");
    return $response=true;

  }

  public function get_data_detail_inbound($po_id){
    $this->db->select('pd.*,p.*');
    $this->db->where('pd.po_id', $po_id);
    $this->db->from('po_detail as pd');
    $this->db->from('products as p');
    $this->db->where('p.product_id=pd.product_id AND p.client_id=pd.client_id');
    $this->db->order_by('pd.line_id', 'desc');
    $data=$this->db->get()->result();
    return $data;
  }

  public function get_detail_inbound_top($po_id){
    $this->db->select('po.*,wtm.*,c.aju_number,c.sppb_number,c.sppb_date,c.bl_awb,c.registration_number,c.registration_date,c.po_invoice_number');
    $this->db->where('po.po_id', $po_id);
    $this->db->from('po as po');
    $this->db->join('po_wtm_status as wtm', 'po.po_status=wtm.status_id', 'left');
    $this->db->join('po_customs as c', 'po.po_id=c.po_id', 'left');
    $data=$this->db->get()->result();
    return $data;
  }

  public function get_detail_gr_top($gr_id){
    $this->db->select('g.*,bp.bp_name,p.*,tr.transporter_name');
    $this->db->where('g.gr_id', $gr_id);
    $this->db->from('gr as g');
    $this->db->join('business_partner as bp', 'bp.bp_id=g.supplier_id', 'left');
    $this->db->join('po as p', 'p.po_id=g.po_id', 'left');
    $this->db->join('transporters as tr', 'tr.transporter_id=g.transporter_id', 'left');
    $data=$this->db->get()->result();
    return $data;
  }

  public function get_detail_gr($gr_id){
    $this->db->select('g.*,p.*');
    $this->db->where('g.gr_id', $gr_id);
    $this->db->from('gr_detail as g');
    $this->db->from('products as p');
    $this->db->where('p.product_id=g.product_id AND p.client_id=g.client_id');
    $this->db->order_by('g.line_id', 'desc');
    $data=$this->db->get()->result();
    return $data;
  }

  public function get_replenish_detail_top($rep_id){
    $this->db->select('repl.*');
    $this->db->where('repl_id', $rep_id);
    $this->db->from('repl');
    $data=$this->db->get()->result();
    return $data;
  }


  public function get_replenish_detail($rep_id){
    $this->db->select('repl.*,p.*');
    $this->db->where('repl.repl_id', $rep_id);
    $this->db->from('repl');
    $this->db->join('mo', 'mo.mo_id=repl.mo_id', 'left');
    $this->db->join('products as p', 'p.product_id=mo.product_id AND p.client_id=mo.client_id', 'left');
    $this->db->order_by('repl.line_id', 'desc');
    $data=$this->db->get()->result();
    return $data;
  }

  public function get_stock_transfer_top($transfer_id){
    $this->db->select('stock_transfer.*,transaction_type.*');
    $this->db->where('transfer_id', $transfer_id);
    $this->db->from('stock_transfer');
    $this->db->join('transaction_type', 'stock_transfer.transaction_type=transaction_type.type_id', 'left');
    $data=$this->db->get()->result();
    return $data;
  }

  public function get_stock_transfer_detail($transfer_id){
    $this->db->select('s.*');
    $this->db->where('s.transfer_id', $transfer_id);
    $this->db->from('stock_transfer_detail as s');
    $data=$this->db->get()->result();
    return $data;
  }

  public function get_stock_check_detail($check_id){
    $this->db->select('stock_check_detail.*,products.*');
    $this->db->where('stock_check_detail.check_id', $check_id);
    $this->db->from('stock_check_detail');
    $this->db->join('products', 'products.product_id=stock_check_detail.product_id', 'left');
    $data=$this->db->get()->result();
    return $data;
  }

  public function get_stock_check_top($check_id){
    $this->db->select('*');
    $this->db->where('check_id', $check_id);
    $this->db->from('stock_check');
    $data=$this->db->get()->result();
    return $data;
  }

  public function get_production_order_top($check_id){
    $this->db->select('p.*,wt.status_description');
    $this->db->where('p.production_id', $check_id);
    $this->db->from('production as p');
    $this->db->join('production_wtm_status as wt', 'wt.status_id=p.production_status', 'left');
    $data=$this->db->get()->result();
    return $data;
  }


  public function get_production_order_detail($check_id){
    $this->db->select('production_detail.*,products.*');
    $this->db->where('production_detail.production_id', $check_id);
    $this->db->from('production_detail');
    $this->db->join('products', 'products.product_id=production_detail.product_id', 'left');
    $this->db->order_by('production_detail.line_id', 'desc');
    $data=$this->db->get()->result();
    return $data;
  }


  public function get_fgr_detail($check_id){
    $this->db->select('fgr_detail.*,products.*');
    $this->db->where('fgr_id', $check_id);
    $this->db->from('fgr_detail');
    $this->db->join('products', 'products.product_id=fgr_detail.product_id', 'left');
    $this->db->order_by('fgr_detail.line_id', 'desc');
    $data=$this->db->get()->result();
    return $data;
  }

  public function get_fgr_top($check_id){
    $this->db->select('f.*,gr.*,production.production_reference');
    $this->db->where('f.fgr_id', $check_id);
    $this->db->from('fgr as f');
    $this->db->join('gr_wtm_status as gr', 'f.gr_put_status=gr.status_id', 'left');
    $this->db->join('production', 'production.production_id=f.production_id', 'left');
    $data=$this->db->get()->result();
    return $data;
  }


  public function get_outbound_delivery_top($check_id){
    $this->db->select('so.*,wt.status_description,bp.bp_id as origin1,bp.bp_name as origin2,bp.address_1 as origin3,bp2.bp_id as destination1,bp2.bp_name as destination2,bp2.address_1 as destination3,c.aju_number,c.sppb_number,c.sppb_date,c.bl_awb,c.registration_number,c.registration_date,c.po_invoice_number');
    $this->db->from('so');
    $this->db->where('so.so_id', $check_id);
    $this->db->join('business_partner as bp', 'bp.bp_id=so.origin_id', 'left');
    $this->db->join('business_partner as bp2', 'bp2.bp_id=so.destination_id', 'left');
    $this->db->join('so_wtm_status as wt', 'wt.status_id=so.so_status', 'left');
    $this->db->join('so_customs as c', 'so.so_id=c.so_id', 'left');
    $data=$this->db->get()->result();
    return $data;
  }

  public function get_outbound_delivery_detail($check_id){
    $this->db->select('so_detail.*,products.*');
    $this->db->where('so_detail.so_id', $check_id);
    $this->db->from('so_detail');
    $this->db->join('products', 'products.product_id=so_detail.product_id', 'left');
    $this->db->order_by('so_detail.line_id', 'desc');
    $data=$this->db->get()->result();
    return $data;
  }

  public function get_delivery_order_top($check_id){
    $this->db->select('dord.*,bp.bp_id as origin1,bp.bp_name as origin2,bp.address_1 as origin3,bp2.bp_id as destination1,bp2.bp_name as destination2,bp2.address_1 as destination3,wt.status_description,so.so_reference,tr.transporter_name');
    $this->db->from('dord');
    $this->db->where('dord.do_id', $check_id);
    $this->db->join('business_partner as bp', 'bp.bp_id=dord.origin_id', 'left');
    $this->db->join('business_partner as bp2', 'bp2.bp_id=dord.destination_id', 'left');
    $this->db->join('dord_wtm_status as wt', 'wt.status_id=dord.do_status', 'left');
    $this->db->join('so', 'so.so_id=dord.so_id', 'left');
    $this->db->join('transporters as tr', 'tr.transporter_id=dord.transporter_id', 'left');
    $data=$this->db->get()->result();
    return $data;
  }


  public function get_delivery_order_detail($check_id){
    $this->db->select('dord_detail.*,p.*');
    $this->db->from('dord_detail');
    $this->db->where('dord_detail.do_id', $check_id);
    $this->db->from('products as p');
    $this->db->where('dord_detail.product_id=p.product_id AND dord_detail.client_id=p.client_id');
    $this->db->order_by('dord_detail.line_id', 'desc');
    $data=$this->db->get()->result();
    return $data;
  }


  public function get_return_delivery_top($check_id){
    $this->db->select('r.*,bp.bp_id as origin1,bp.bp_name as origin2,bp.address_1 as origin3,bp2.bp_id as destination1,bp2.bp_name as destination2,bp2.address_1 as destination3,wt.status_description,d.do_reference');
    $this->db->where('r.rdo_id', $check_id);
    $this->db->from('rdo as r');
    $this->db->join('business_partner as bp', 'bp.bp_id=r.origin_id', 'left');
    $this->db->join('business_partner as bp2', 'bp2.bp_id=r.destination_id', 'left');
    $this->db->join('rdo_wtm_status as wt', 'wt.status_id=r.rdo_status', 'left');
    $this->db->join('dord as d', 'd.do_id=r.do_id', 'left');
    $data=$this->db->get()->result();
    return $data;
  }

  public function get_return_delivery_detail($check_id){
    $this->db->select('r.*,p.*');
    $this->db->from('rdo_detail as r');
    $this->db->where('r.rdo_id', $check_id);
    $this->db->from('products as p');
    $this->db->where('r.product_id=p.product_id AND r.client_id=p.client_id');
    $this->db->order_by('r.line_id', 'desc');
    $data=$this->db->get()->result();
    return $data;
  }


  public function get_client_list(){
    $this->db->from('clients');
    $data=$this->db->get()->result();
    return $data;
  }

  public function get_client_list_checked($id){
    $this->db->from('user_client');
    $this->db->where('user_id', $id);
    $data=$this->db->get()->result();
    return $data;
  }

}
?>
